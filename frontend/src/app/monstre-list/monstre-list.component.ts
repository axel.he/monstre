import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Monstre } from '../models/monstre';
import { MonstreService } from '../services/monstre.service';

@Component({
  selector: 'app-monstre-list',
  templateUrl: './monstre-list.component.html',
  styleUrls: ['./monstre-list.component.css']
})
export class MonstreListComponent implements OnInit{

  monstre!: Monstre[];

  constructor(private monstreService: MonstreService, private router: Router){}

  ngOnInit(): void {
    this.monstreService.getAllMonstre().subscribe(monstreList => {this.monstre = monstreList});
  }

  goToDetail(monstre: Monstre){
    this.router.navigate(['/monstre-detail', monstre.login]);
  }

  goToDelete(){
    this.monstreService.deleteMonstre(this.monstre.login).subscribe(
      response =>{
        console.log(response);
        this.router.navigate(['/monstre-list']);
      },
      error => {
        console.log(error);
      }
    );
  }
}
