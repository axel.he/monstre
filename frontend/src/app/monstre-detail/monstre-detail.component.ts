import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Monstre } from '../models/monstre';
import { MonstreService } from '../services/monstre.service';

@Component({
  selector: 'app-monstre-detail',
  templateUrl: './monstre-detail.component.html',
  styleUrls: ['./monstre-detail.component.css']
})
export class MonstreDetailComponent implements OnInit{

  monstre: Monstre | undefined;

  constructor(private monstreService: MonstreService, private route: ActivatedRoute ){}

  ngOnInit(): void {

    const monstreId: string | null = this.route.snapshot.paramMap.get('login');
    console.log(monstreId);

    if(monstreId){
      this.monstreService.getOneMonstre(monstreId)
      .subscribe(monstre => this.monstre = monstre);
    }
  }

}
