import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginAndRegistrationComponent } from './login-and-registration/login-and-registration.component';
import { MonstreListComponent } from './monstre-list/monstre-list.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AjoutMonstreComponent } from './ajout-monstre/ajout-monstre.component';
import { MonstreDetailComponent } from './monstre-detail/monstre-detail.component';
import { CNavComponent } from './nav/nav.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginAndRegistrationComponent,
    MonstreListComponent,
    AjoutMonstreComponent,
    MonstreDetailComponent,
    CNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
