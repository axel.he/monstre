import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AjoutMonstreComponent } from './ajout-monstre/ajout-monstre.component';
import { LoginAndRegistrationComponent } from './login-and-registration/login-and-registration.component';
import { MonstreDetailComponent } from './monstre-detail/monstre-detail.component';
import { MonstreListComponent } from './monstre-list/monstre-list.component';

const routes: Routes = [
  { path: 'login-and-registration', component: LoginAndRegistrationComponent},
  { path: 'monstre-list', component: MonstreListComponent},
  { path: 'ajout-monstre', component: AjoutMonstreComponent},
  { path: 'monstre-detail/:login', component: MonstreDetailComponent},
  {path: '', redirectTo: '/monstre-list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
