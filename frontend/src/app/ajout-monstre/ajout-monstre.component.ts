import { Component, OnInit } from '@angular/core';
import { MonstreService } from '../services/monstre.service';
import { Monstre } from '../models/monstre';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs';

@Component({
  selector: 'app-ajout-monstre',
  templateUrl: './ajout-monstre.component.html',
  styleUrls: ['./ajout-monstre.component.css']
})
export class AjoutMonstreComponent implements OnInit{

  addForm!: FormGroup;
  monstre!: Monstre[];

  constructor(
    private monstreService: MonstreService,
    private formBuilder: FormBuilder,
    private router: Router,
    )
    {}

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      login: ['', Validators.required],
      mdp: ['', Validators.required],
      nom: ['', Validators.required],
      imageUrl: ['', Validators.required],
      role: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  get f() { return this.addForm.controls; }

  createMonstre(): void{

    if(this.addForm.invalid){
      return;
    }
    this.monstreService.createMonstre(this.f['login'].value, this.f['mdp'].value, this.f['nom'].value, this.f['imageUrl'].value , this.f['role'].value, this.f['description'].value)
    .subscribe({
      next: () =>{
        this.router.navigate(['/']);
      }
    });
  }
}
