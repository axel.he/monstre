import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, Observable, of, tap } from 'rxjs';
import { Monstre } from '../models/monstre';

@Injectable({
  providedIn: 'root'
})
export class MonstreService {

  private monstreUrl: string;

  constructor(private http: HttpClient) {
    this.monstreUrl = 'http://localhost:3000'
   }

  private log(response: any ) {
    console.table(response);
  }
  private handleError(error: Error, errorValue: any){
    console.error(error);
    return of(errorValue);
  }

public getOneMonstre(login: string): Observable<any> {
    return this.http.get(this.monstreUrl+`/monstre/${login}`).pipe(
      tap((response)=>this.log(response)),
      catchError((error)=>this.handleError(error, []))
    );
}

public getAllMonstre(): Observable<Monstre[]> {
  return this.http.get<Monstre[]>(this.monstreUrl+'/monstre').pipe(
    tap((response)=>this.log(response)),
    catchError((error)=>this.handleError(error, []))
  );
}

// public deleteMonstre(): Observable<Monstre[]> {
//   return this.http.delete<Monstre[]>(this.monstreUrl+'/monstre/:login').pipe(
//     tap((response)=>this.log(response)),
//     catchError((error)=>this.handleError(error, []))
//   );
// }

public deleteMonstre(login: string): Observable<any>{
  return this.http.delete(`${this.monstreUrl}/${login}`);
}

public modifyMonstre(): Observable<Monstre[]> {
  return this.http.get<Monstre[]>(this.monstreUrl+'/monstre/:id').pipe(
    tap((response)=>this.log(response)),
    catchError((error)=>this.handleError(error, []))
  );
}

public createMonstre(login: string, mdp: string, nom: string,imageUrl: string, role: string, description: string): Observable<any> {
  return this.http.post(this.monstreUrl+'/monstre', {login, mdp, nom, imageUrl,role, description});
}



}
