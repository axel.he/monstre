import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, Observable, of, tap } from 'rxjs';
import { Monstre } from '../models/monstre';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private monstreUrl: string;

  constructor(private http: HttpClient) {
    this.monstreUrl = 'http://localhost:3000/api/monstre'
   }

  private log(response: any ) {
    console.table(response);
  }
  private handleError(error: Error, errorValue: any){
    console.error(error);
    return of(errorValue);
  }



  public signup(): Observable<Monstre[]> {
    return this.http.get<Monstre[]>(this.monstreUrl+'/monstre/signup').pipe(
      tap((response)=>this.log(response)),
      catchError((error)=>this.handleError(error, []))
    );
  }

  public login(): Observable<Monstre[]> {
    return this.http.get<Monstre[]>(this.monstreUrl+'/monstre/login').pipe(
      tap((response)=>this.log(response)),
      catchError((error)=>this.handleError(error, []))
    );
  }

}
