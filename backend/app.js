const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const monstreRoutes = require('./routes/monstreRoutes');

mongoose.connect('mongodb+srv://axel:axel@cluster0.zspbyne.mongodb.net/?retryWrites=true&w=majority', 
{useNewUrlParser: true, useUnifiedTopology: true})
.then( ()=> console.log('Connexion à MongoDB réussi'))
.catch(() => console.log('Connexion à mongoDB échoué'));

const app = express();

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());



app.use('/monstre', monstreRoutes);

module.exports = app;