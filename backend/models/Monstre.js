const mongoose = require('mongoose');

const monstreSchema = mongoose.Schema({
    nom: { type: String, required: true},
    role: { type: String, required: true},
    imageUrl: { type: String, required: true},
    description: { type: String, required: true},
    login: { type: String, required: true},
    mdp: { type: String, required: true}
})

module.exports = mongoose.model('Monstre', monstreSchema);