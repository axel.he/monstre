const auth = require('../middleware/auth');
const Monstre = require('../models/Monstre');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const fs = require('fs');

exports.createMonstre = (req, res, next) => {
  // const monstreObject = JSON.parse(req.body.monstre);
  // delete monstreObject._id;
  // delete monstreObject._userId;
  // const monstre = new Monstre({
  //     ...monstreObject,
  //     userId: req.auth.userId,
  //     imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
  // });
  // monstre.save()
  // .then(() => { res.status(201).json({message: 'Monstre enregistré !'})})
  // .catch(error => { res.status(400).json( { error })})
    delete req.body._id;
    const monstre = new Monstre({...req.body});
    monstre.save()
      .then( monstre => res.status(201).json({ monstre }))
      .catch(error => res.status(400).json({ error }));
};

exports.getOneMonstre = (req, res, next) => {
  // Monstre.findOne({
  //   _id: req.params.id
  // }).then(
  //   (monstre) => {
  //     res.status(200).json(monstre);
  //   }
  // ).catch(
  //   (error) => {
  //     res.status(404).json({
  //       error: error
  //     });
  //   }
  // );
  Monstre.findOne({ _id: req.params.login })
  .then(monstre => res.status(200).json(monstre))
  .catch(error => res.status(404).json({ error }));
};

exports.modifyMonstre = (req, res, next) => {
  // const monstreObject = req.file ? {
  //   ...JSON.parse(req.body.monstre),
  //   imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
  // } : { ...req.body };
  // delete monstreObject._userId;
  // Monstre.findOne({_id: req.params.id})
  // .then((monstre) => {
  //   if(monstre.userId != req.auth.userId){
  //     res.status(401).json({ message: 'Non-autorisé' })
  //   }else{
  //     Monstre.updateOne({ _id: req.params.id}, { ...monstreObject, _id: req.params.id})
  //     .then(() => res.status(200).json({ message: 'Monstre modifié'}))
  //     .catch(error => res.status(401).json({ error }));
  //   }
  // })
  // .catch((error) => {res.status(400).json({ error });});
  Monstre.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
    .then(monstre => res.status(200).json({ message: 'Monstre modifié'}))
    .catch(error => res.status(400).json({ error }));
};

exports.deleteMonstre = (req, res, next) => {
  // Monstre.findOne({ _id: req.params.id})
  // .then( monstre => {
  //   if(monstre.userId != req.auth.userId){
  //     res.status(401).json({ message: 'Non-autorisé' });
  //   }else{
  //     const filename = monstre.imageUrl.split('/images/')[1];
  //     fs.unlink(`images/${filename}`, () => {
  //       Monstre.deleteOne({_id: req.params.id})
  //       .then(() => { res.status(200).json({ message: 'Monstre supprimé'});})
  //       .catch( error => res.status(401).json({ error }));
  //     })
  //   }
  // })
  // .catch( error => {
  //   res.status(500).json({ error });
  // });
  Monstre.deleteOne({ login: req.params.login })
    .then(() => res.status(200).json({ message: 'Monstre supprimé !'}))
    .catch(error => res.status(400).json({ error }));
};

exports.getAllMonstre = (req, res, next) => {
  Monstre.find().then(
    (monstres) => {
      res.status(200).json(monstres);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.signup = (req, res, next) => {
  console.log(req.body);
  bcrypt.hash(req.body.password, 10)
    .then(hash => {
      const user = new user({
        email: req.body.email,
        password: hash
      });
      user.save()
        .then(() => res.status(201).json({ message: 'Utilisateur créé !' }))
        .catch(error => res.status(400).json({ error }));
    })
    .catch(error => res.status(500).json({ error }));
};

exports.login = (req, res, next) => {
  console.log(req);
  User.findOne({ email: req.body.email })
      .then(user => {
          if (!user) {
              return res.status(401).json({ message: 'Paire login/mot de passe incorrecte'});
          }
          bcrypt.compare(req.body.password, user.password)
              .then(valid => {
                  if (!valid) {
                      return res.status(401).json({ message: 'Paire login/mot de passe incorrecte' });
                  }
                  res.status(200).json({
                      userId: user._id,
                      token: jwt.sign(
                          { userId: user._id },
                          'RANDOM_TOKEN_SECRET',
                          { expiresIn: '24h' }
                      )
                  });
              })
              .catch(error => res.status(500).json({ error }));
      })
      .catch(error => res.status(500).json({ error }));
};