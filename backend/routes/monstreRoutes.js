const express = require('express');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');
const router = express.Router();

const monstreCtrl = require('../controllers/monstreCtrl')

router.post('/',  monstreCtrl.createMonstre);  
router.put('/:login',  monstreCtrl.modifyMonstre);
router.delete('/:login',  monstreCtrl.deleteMonstre);
router.get('/:login', monstreCtrl.getOneMonstre);
router.get('/',  monstreCtrl.getAllMonstre);
router.post('/signup', monstreCtrl.signup);
router.post('/login', monstreCtrl.login);

module.exports = router;